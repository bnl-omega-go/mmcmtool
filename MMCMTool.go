//go:build linux || darwin
// +build linux darwin

package main

import (
	"flag"
	"fmt"
	"log"
	"math"
	"time"

	axi "gitlab.cern.ch/bnl-omega-go/axi"
)

func GetPhase(phase_in_deg float64) uint32 {
	return uint32(math.Floor(phase_in_deg * 1000))
}

//Convenient struct ot list registers
type Register struct {
	name   string
	offset int
	mode   int
}

// Register list, offset given in 32bits units for AXI linrary
var MMCM_Registers []Register = []Register{
	{"PHASE_CLK0", 0x20c / 4, axi.AXIRW},
	{"PHASE_CLK1", 0x218 / 4, axi.AXIRW},
	{"PHASE_CLK2", 0x224 / 4, axi.AXIRW},
	{"PHASE_CLK3", 0x230 / 4, axi.AXIRW},
	{"PHASE_CLK4", 0x23C / 4, axi.AXIRW},
	{"PHASE_CLK5", 0x248 / 4, axi.AXIRW},
	{"PHASE_CLK6", 0x254 / 4, axi.AXIRW},
	{"CONFIG_LOAD", 0x25C / 4, axi.AXIRW},
	{"STATUS", 0x4 / 4, axi.AXIRW},
}

func main() {

	base := flag.Int64("b", 0x00A0010000, "Base address of the AXI register table (ex: 0x3000)")
	length := flag.Int("l", 4096, "length of the AXI register table (ex: 4096)")
	verbose := flag.Bool("v", false, "print extra verbosity during execution")
	clock := flag.Int("c", 0, "Clock to program (0-6)")
	phase := flag.Float64("p", 0, "Phase in degrees (LSB=0.001°)")

	flag.Parse()

	//Check arguments are within bounds
	if (*phase < 0) || (*phase > 360) {
		log.Fatal("Please provide a valid phase :  0° < phase < 360°")
	}
	if (*clock < 0) || (*clock > 6) {
		log.Fatal("Please provide a valid clock : 0-6")
	}

	// Book MMCM AXI Register page and book individual logical registers by name
	pages := map[string]axi.Page{"MMCM": axi.Page{Base: *base, Length: *length}}
	ctrl := axi.AXIController{}
	ctrl.BookPages(pages)
	for _, reg := range MMCM_Registers {
		ctrl.BookRegister("MMCM", reg.name, reg.offset, reg.mode)
	}

	//Write Phase value to selected clock
	phase_reg := GetPhase(*phase)
	log.Printf("Writing phase %v° (%v) to MMCM clock #%v at 0x%x", *phase, phase_reg, *clock, *base)

	start := time.Now()
	clk_phase_reg := fmt.Sprintf("PHASE_CLK%v", *clock)
	ctrl.Write(clk_phase_reg, phase_reg)
	ctrl.Write("CONFIG_LOAD", 0b111)
	ctrl.Write("CONFIG_LOAD", 0b010)

	//Read back Phase value to selected clock
	phase_rb := float64(ctrl.Read(clk_phase_reg)) / 1000.

	//Print readback and duration if verbose on
	if *verbose {
		log.Printf("Reading back phase %v°", phase_rb)
		log.Printf("%s for operation", time.Since(start))
	}

	// Verify PLL lock status before quitting

	lock := ctrl.Read("STATUS")
	if lock == 1 {
		log.Println("PLL Locked verified")
	} else {
		log.Println("PLL not locked")
	}

	ctrl.Close()

}
