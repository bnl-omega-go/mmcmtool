module gitlab.cern.ch/bnl-omega-go/MMCMTool

go 1.17

require gitlab.cern.ch/bnl-omega-go/axi v0.0.0-20220430123121-5ef3cd149a51

require (
	github.com/codehardt/mmap-go v1.0.1 // indirect
	golang.org/x/sys v0.0.0-20220209214540-3681064d5158 // indirect
)
