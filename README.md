# Standalone tool for the configuration of Xilinx MMCM 

This tool allows to configure the phase of a Xilinx MMCM via the OS

## Compilation

To compile : 

```
go mod tidy
go build MMCMTool.go 
```

To compile for a different platform, most likely arm64/linux : 

```
go mod tidy 
GOOS=linux GOARCH=arm64 go build MMCMTool.go
```

The produced executable can be transfered to your arm platform for usage. 

## Usage 

You can use the -h option of the executable to see the available parameters : 

```
./MMCMTool -h
Usage of ./MMCMTool:
  -b int
    	Base address of the AXI register table (ex: 0x3000) (default 2684420096)
  -c int
    	Clock to program (0-6)
  -l int
    	length of the AXI register table (ex: 4096) (default 4096)
  -p float
    	Phase in degrees (LSB=0.001°)
  -v	print extra verbosity during execution

  ```


## Precompiled binaries 

You can always download the precompiled binaries from the artifacts produced during CI  : 

* [amd64](https://gitlab.cern.ch/bnl-omega-go/mmcmtool/-/jobs/artifacts/master/raw/MMCMTool?job=build-job)
* [arm32](https://gitlab.cern.ch/bnl-omega-go/mmcmtool/-/jobs/artifacts/master/raw/MMCMTool_arm32?job=build_arm32-job)
* [arm64](https://gitlab.cern.ch/bnl-omega-go/mmcmtool/-/jobs/artifacts/master/raw/MMCMTool_arm64?job=build_arm64-job)


